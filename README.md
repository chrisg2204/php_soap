# WEB SERVICE SOAP (PHP)

Para ejecutar el WS es necesario configurar las variables
de conexión a la base de datos en `config.php`.

Dentro de la carpeta `/xml` estan los objetos, entrar y luego ejecutar los comandos según se requiera.

#### CREATE
```sh
$ curl -X POST -H "Content-Type: text/xml" -H 'SOAPAction: "http://www.localhost.com/userservice#addUser"' --data @add.xml http://127.0.0.1/php_soap_service/server.php?awsdl
```

#### DESACTIVATE
```sh
$ curl -X POST -H "Content-Type: text/xml" -H 'SOAPAction: "http://www.localhost.com/userservice#desactivateUser"' --data @desactivate.xml http://127.0.0.1/php_soap_service/server.php?awsdl
```

#### ACTIVATE
```sh
$ curl -X POST -H "Content-Type: text/xml" -H 'SOAPAction: "http://www.localhost.com/userservice#activateUser"' --data @activate.xml http://127.0.0.1/php_soap_service/server.php?awsdl
```

#### GET
```sh
$ curl -X POST -H "Content-Type: text/xml" -H 'SOAPAction: "http://www.localhost.com/userservice#getUser"' --data @get.xml http://127.0.0.1/php_soap_service/server.php?awsdl
```