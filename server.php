<?php

require_once "lib/nusoap.php";
require_once "config.php";
require_once "pdo_db.php";
 
class User {

    /**
     * [addUser description]
     * @param string $username [description]
     * @param string $password [description]
     * @param string $email    [description]
     */
    public function addUser(string $username, string $password, string $email) {

        $pdo = new PDO_DB();

        openlog("myprogram", 0, LOG_LOCAL0);
        syslog(LOG_INFO, "INFO - Ejecutando function addUser");

        $sqlFind = "SELECT id FROM `".DATABASE_NAME."`.`usuarios` where username = '".$username."' LIMIT 1;";
        $execSqlFind = $pdo->execSelectQuery($sqlFind);

        if (count($execSqlFind)) {
            $strFormat = "ERROR - Nombre de usuario '%s' ya existe";
            $strError = sprintf($strFormat, $username);

            syslog(LOG_ERR, $strError);
            error_log($strError);

            return 0;
        } else {

            $sha1Passwd = sha1($password);
            $reqParams = array($username, $sha1Passwd, $email);
            $sqlInsert = "INSERT INTO `".DATABASE_NAME."`.`usuarios` (username, password, email) VALUES (?, ?, ?)";
            $typesParams = array(PDO::PARAM_STR, PDO::PARAM_STR, PDO::PARAM_STR);

            if ($pdo->execQuery($sqlInsert, $typesParams, $reqParams)) {
                $strFormat = "INFO - Procesamos request addUser | usuario: %s | password: %s | email: %s";
                $strInfo = sprintf($strFormat, $username, $sha1Passwd, $email);

                syslog(LOG_INFO, $strInfo);
                return 0;
            } else {
                error_log("Erro al ejecutar sql.");
            }
        }
        closelog();
    }

    /**
     * [desactivateUser description]
     * @param  string $username [description]
     * @return [type]           [description]
     */
    public function desactivateUser(string $username) {

        $pdo = new PDO_DB();

        openlog("myprogram", 0, LOG_LOCAL0);
        syslog(LOG_INFO, "INFO - Ejecutando function desactivateUser");

        $sqlFind = "SELECT id FROM `".DATABASE_NAME."`.`usuarios` where username = '".$username."' LIMIT 1;";
        $execSqlFind = $pdo->execSelectQuery($sqlFind);

        if (count($execSqlFind)) {

            $reqParams = array($username);
            $sqlUpdate = "UPDATE `".DATABASE_NAME."`.`usuarios` SET status = 0 where username = ?";
            $typesParams = array(PDO::PARAM_STR);

            if ($pdo->execQuery($sqlUpdate, $typesParams, $reqParams)) {
                $strFormat = "INFO - Procesamos request desactivateUser | usuario: %s";
                $strInfo = sprintf($strFormat, $username);

                syslog(LOG_INFO, $strInfo);

                return 0;
            } else {
                error_log("Erro al ejecutar sql.");
            }
        }

        closelog();
    }

    /**
     * [activateUser description]
     * @param  string $username [description]
     * @return [type]           [description]
     */
    public function activateUser(string $username) {

        $pdo = new PDO_DB();

        openlog("myprogram", 0, LOG_LOCAL0);
        syslog(LOG_INFO, "INFO - Ejecutando function activateUser");

        $sqlFind = "SELECT id FROM `".DATABASE_NAME."`.`usuarios` where username = '".$username."' LIMIT 1;";
        $execSqlFind = $pdo->execSelectQuery($sqlFind);

        if (count($execSqlFind)) {

            $reqParams = array($username);
            $sqlUpdate = "UPDATE `".DATABASE_NAME."`.`usuarios` SET status = 1 where username = ?";
            $typesParams = array(PDO::PARAM_STR);

            if ($pdo->execQuery($sqlUpdate, $typesParams, $reqParams)) {
                $strFormat = "INFO - Procesamos request activateUser | usuario: %s";
                $strInfo = sprintf($strFormat, $username);

                syslog(LOG_INFO, $strInfo);

                return 0;
            } else {
                error_log("Erro al ejecutar sql.");
            }
        }

        closelog();
    }

    /**
     * [getUser description]
     * @param  string $username [description]
     * @return [type]           [description]
     */
    public function getUser(string $username) {

        $pdo = new PDO_DB();

        openlog("myprogram", 0, LOG_LOCAL0);
        syslog(LOG_INFO, "INFO - Ejecutando function getUser");

        $sqlFind = "SELECT username, password, email FROM `".DATABASE_NAME."`.`usuarios` where username = '".$username."' LIMIT 1;";
        $execSqlFind = $pdo->execSelectQuery($sqlFind);

        foreach ($execSqlFind as $key => $value) {
            $strFormat = "INFO - Procesamos request getUser | usuario: %s";
            $strInfo = sprintf($strFormat, $value["username"]);

            syslog(LOG_INFO, $strInfo);
        }

        closelog();

        return $execSqlFind;
    }

}

$server = new soap_server();
$server->configureWSDL("userservice", "http://www.localhost.com/userservice");

$server->register("User.addUser",
    array(
        "username" => "xsd:string",
        "password" => "xsd:string",
        "email" => "xsd:string",
    ),
    array('return' => 'xsd:string'),
    'http://www.localhost.com/userservice',
    'http://www.localhost.com/userservice#addUser',
    'rpc',
    'encoded',
    '');

$server->register("User.desactivateUser",
    array(
        "username" => "xsd:string",
    ),
    array('return' => 'xsd:string'),
    'http://www.localhost.com/userservice',
    'http://www.localhost.com/userservice#desactivateUser',
    'rpc',
    'encoded',
    '');

$server->register("User.activateUser",
    array(
        "username" => "xsd:string",
    ),
    array('return' => 'xsd:string'),
    'http://www.localhost.com/userservice',
    'http://www.localhost.com/userservice#activateUser',
    'rpc',
    'encoded',
    '');

$server->wsdl->addComplexType(
    'Busca',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' => array('name' => 'username', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'),
        'email' => array('name' => 'email', 'type' => 'xsd:string'),
    ));

$server->wsdl->addComplexType(
    "TableArray",
    "complexType",
    "array",
    "",
    "SOAP-ENC:Array",
    array("username" => "xsd:string"),
    array(
        'Job' => array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Busca[]')
    ),
    'tns:Busca');

$server->register("User.getUser",
    array("username" => "xsd:string"),
    array("name" => "tns:TableArray"),
    'http://www.localhost.com/userservice',
    'http://www.localhost.com/userservice#getUser',
    'rpc',
    'encoded',
    ''
    );
 
@$server->service(file_get_contents("php://input"));
